import beans.Employee;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class EmployeeTest {

    private ClassPathXmlApplicationContext context;

    @BeforeMethod
    public void init() {
        context = new ClassPathXmlApplicationContext("spring.xml");
    }

    @AfterMethod
    public void tearDown() {
        context.close();
    }

    @Test
    public void appTest() {

//      По-хорошему, вместо захардкоживания констант, их надо бы вынести в property файл...

        Employee employee = (Employee) context.getBean("employee");

        assertNotNull(employee.getDepartment());
        assertNotNull(employee.getPosition());

        assertEquals(employee.getDepartment().getId(), 1);
        assertEquals(employee.getDepartment().getName(), "Development");

        assertEquals(employee.getPosition().getId(), 2);
        assertEquals(employee.getPosition().getName(), "Middle Java Developer");

        assertEquals(employee.getId(), 1);
        assertEquals(employee.getFirstName(), "John");
        assertEquals(employee.getLastName(), "Smith");
    }

}