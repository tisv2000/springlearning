#Spring Learning 
##Prerequisites

* Java 8
* Maven

##Getting the project and run unit test

    cd <your projects list folder>
    
    # Use bitbucket.org OR github.com repository
    # git clone https://bitbucket.org/tisv2000/springlearning.git SpringLearning
    git clone https://github.com/tisv2000/SpringLearning.git SpringLearning
    
    cd SpringLearning
    mvn clean test